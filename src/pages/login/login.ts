import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { TabsPage } from "../tabs/tabs";
import { Storage } from "@ionic/storage";

/**
 * Generated class for the Login page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {

  usuario: any = {
    username: "",
    password: ""
  };

  constructor(public navCtrl: NavController, public navParams: NavParams,
  private toastCtrl: ToastController, public storage: Storage) {
  }

  ionViewWillEnter(){
    //this.storage.clear();
    this.storage.get("user")
                .then(user=>{ console.log("user: "+user);if(user != null ){this.navCtrl.setRoot(TabsPage);}})
                .catch(error => {console.log(error)});
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Login');
  }

  llevameSinRegreso(){
    //if(this.usuario.username == "pabhoz" && this.usuario.password == "1234"){
      let user: String = this.usuario.username;
      this.storage.set("user", user);
      this.navCtrl.setRoot(TabsPage);
    /*}else{
      this.presentToast("Usted no es pabhoz",3000,"middle")
    }*/
    
  }

  presentToast(msg,time,pos) {
  let toast = this.toastCtrl.create({
    message: msg,
    duration: time,
    position: pos
  });

  toast.onDidDismiss(() => {
    console.log('Dismissed toast');
  });

  toast.present();
}

}
