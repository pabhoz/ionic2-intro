import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { NgForm } from "@angular/forms";
import { VideoService } from "../../services/video.service";

/**
 * Generated class for the Upload page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-upload',
  templateUrl: 'upload.html',
})
export class Upload {

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public viewCtrl: ViewController, public videoService: VideoService) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad Upload');
    //Vamos a sobreescribir el botón "back" por "cancelar"
    this.viewCtrl.setBackButtonText("Cancelar");
  }

  uploadVideo(formulario: NgForm){
    console.log(formulario.value),
    this.videoService.uploadVideo(formulario.value.titulo,formulario.value.tags,formulario.value.file);
    formulario.reset();
    this.viewCtrl.dismiss();
  }

}
