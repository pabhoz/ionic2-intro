import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Upload } from "../upload/upload";
import { Video } from "../../classes/video";
import { VideoService } from "../../services/video.service";
import { Storage } from "@ionic/storage";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  uploadPage: any = Upload;
  videos: Video[];
  username: String;

  rusties: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
  public videoService: VideoService, public storage:Storage) {}

  ionViewWillEnter(){
    this.storage.get("user")
                .then(user=>{this.username = user})
                .catch(e => console.log(e));
    this.videos = this.videoService.getVideos();
    this.videoService.test().then(data => this.rusties = data);
  }

  ionViewDidLoad(){
  }

}
