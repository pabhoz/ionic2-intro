import { Injectable } from "@angular/core";
import { Video } from "../classes/video";

import { Http } from "@angular/http";
import 'rxjs';

import { config } from "./config";

@Injectable()
export class VideoService{

    videos: Video[] = [];
    rusties: any = [];

    constructor(private http: Http){}

    uploadVideo(titulo:String,tags:String,file:String){
        let sepTags = [tags];
        let video = new Video(titulo,sepTags,file);
        this.videos.push(video);
        console.log(this.videos);
    }

    getVideos(){
        //usamos el método slice del arreglo para devolver
        //una copia en lugar del original
        return this.videos.slice();
    }

    test(){
        var url = config.SERVICES+'Rusty';
        return this.http.get(url)
                 .toPromise()
                 .then( data => data.json(), err => console.log(err));
    }

}