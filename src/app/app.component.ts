import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from "@ionic/storage";

import { TabsPage } from '../pages/tabs/tabs';
import { Login } from "../pages/login/login";
import { Registro } from "../pages/registro/registro";

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  //rootPage:any = TabsPage;
  rootPage:any = Login;

  @ViewChild(Nav) nav: Nav;

  appPages: any[] = [
    { title: 'Cerrar Sesión', action: "closeSession", icon: 'exit' }
  ];

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
  public menu: MenuController, public storage: Storage) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  openPage(page) {
    this.menu.close();
    if(page.action != null){
      switch (page.action) {
        case "closeSession":
            console.log(this.storage); 
            this.storage.clear();
            this.nav.setRoot(Login);
          break;
      
        default:
          break;
      }
    }else{
      this.nav.setRoot(page.component, page.title);
    }
  }
}
